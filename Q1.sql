create database sql_tasc DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
-- データベース使用状態
use sql_tasc;

-- -------------------------------------------------------
-- テーブル　item_category

create table item_category (
	category_id int NOT NULL AUTO_INCREMENT primary key,
	category_name VARCHAR(256) NOT NULL
);

insert into item_category(category_id,category_name) values (1,'家具');
insert into item_category(category_id,category_name) values (2,'食品');
insert into item_category(category_id,category_name) values (3,'本');

-- -------------------------------------------------------
-- テーブル　item

create table item (
  item_id int NOT NULL AUTO_INCREMENT primary key,
  item_name VARCHAR(256) NOT NULL,
  Item_price int NOT NULL,
  category_id int
);

insert into item(item_id,item_name,Item_price,category_id) values (1,"堅牢な机",3000,1);
insert into item(item_id,item_name,Item_price,category_id) values (2,"生焼け肉",50,2);
insert into item(item_id,item_name,Item_price,category_id) values (3,"すっきりわかるJava入門",3000,3);

insert into item(item_id,item_name,Item_price,category_id) values (4,"おしゃれな椅子",2000,1);
insert into item(item_id,item_name,Item_price,category_id) values (5,"こんがり肉",500,2);
insert into item(item_id,item_name,Item_price,category_id) values (6,"書き方ドリルSQL",2500,3);

insert into item(item_id,item_name,Item_price,category_id) values (7,"ふわふわのベッド",30000,1);
insert into item(item_id,item_name,Item_price,category_id) values (8,"ミラノ風ドリア",300,2);

-- -------------------------------------------------------
-- カテゴリー1(家具)の item_nameとitem_priceを検索

SELECT item_name, item_price
FROM item
WHERE category_id = 1;

-- -------------------------------------------------------
-- item_priceが1000以上のitem_nameとitem_priceを検索

SELECT item_name, item_price
FROM item
WHERE item_price >= 1000;

-- -------------------------------------------------------
-- item_nameに "肉" が含まれている item_name, item_priceを検索

SELECT item_name, item_price
FROM item
WHERE item_name LIKE '%肉%';


-- -------------------------------------------------------
-- itemテーブルとitem_categoryテーブルを結合
-- 取得するカラムは「item_id」、「item_name」 、「item_price」 、「category_name」

SELECT item_id, item_name, item_price, category_name
FROM
    item
INNER JOIN
    item_category
ON
    item.category_id = item_category.category_id;


-- -------------------------------------------------------
-- itemテーブルとitem_categoryテーブルを結合する
-- テーブルを、「 category_id 」単位で集計する
-- 取得するカラムは「category_name」、「 item_price の合計(total_priceという別名をつける)」
-- total_priceの値が大きな順にレコードを並び替える

SELECT category_name, SUM(item_price) AS total_price
FROM
    item
INNER JOIN
    item_category
ON
    item.category_id = item_category.category_id
GROUP BY
    category_name











-- -------------------------------------------------------
